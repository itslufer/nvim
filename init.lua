--  ============================================================================
--  ███╗   ██╗  | Minha configuração do Neovim
--  ████╗  ██║  | 
--  ██╔██╗ ██║  |
--  ██║╚██╗██║  | 
--  ██║ ╚████║  | 
--  ╚═╝  ╚═══╝  |
--  ============================================================================
-- Fundamental
require 'settings'
require 'finder'
require 'keymaps'
require 'colors'

-- Administradores de Plugins
require 'vim-plug'
require 'vim-packer'

-- LSP stuff
-- require 'lsp-config'
-- require 'lsp-cmp'

-- Plugins específicos
-- require 'auto-pairs'
require 'colorizer-config'

-- Temas
require 'gruvbox-colors'
-- require 'doom-one-config'
