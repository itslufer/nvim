-- Load the colorscheme
vim.cmd[[let g:gruvbox_contrast_dark = 'medium']]
vim.cmd[[let g:gruvbox_contrast_light='hard']]
vim.cmd[[colorscheme gruvbox]]
vim.cmd[[hi LspCxxHlGroupMemberVariable guifg=#83a598]]
