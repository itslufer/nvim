vim.cmd ([[
 
    let g:netrw_preview = 1
    let g:netrw_preview   = 1
    let g:netrw_liststyle = 3
    let g:netrw_winsize   = 30

    cabbr <expr> here expand('%:p:h')

  ]])

