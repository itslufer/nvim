local keymap = vim.api.nvim_set_keymap
local ns = { noremap = true, silent = true }

-- INSERT ---------------------------------------------------------------------

-- Quit insert mode faster
keymap("i", "kq", "<Esc>", ns)

-- Navigate on insert mode
keymap("i", "<A-h>", "<Esc>i", ns)
keymap("i", "<A-l>", "<Esc>la", ns)
keymap("i", "<A-j>", "<Esc>ja", ns)
keymap("i", "<A-k>", "<Esc>ka", ns)

-- NORMAL ---------------------------------------------------------------------

-- Ergonomical A command
keymap("n", "<leader>a", "A", ns)

-- Better page down and page up 
keymap("n", "<A-k>", "6k", ns)
keymap("n", "<A-j>", "6j", ns)

-- Easy splits
keymap("n", "<leader>l", ":echo 'Waiting for file...'<CR>:vsplit ", ns)
keymap("n", "<leader>j", ":echo 'Waiting for file...'<CR>:split ", ns)

-- Better splits movement
keymap("n", "<C-l>", "<C-w>l", ns)
keymap("n", "<C-k>", "<C-w>k", ns)
keymap("n", "<C-j>", "<C-w>j", ns)
keymap("n", "<C-h>", "<C-w>h", ns)
keymap("n", "<C-l>", "<C-w>l", ns)

-- Resize splits with arrows
keymap("n", "<C-Up>", ":resize -2<CR>", ns)
keymap("n", "<C-Down>", ":resize +2<CR>", ns)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", ns)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", ns)

-- Little terminal
keymap("n",
   "<leader>t",
    ":split<CR>:term<CR>:resize 10<CR>i",
    ns)

-- Reload luafile
keymap("n",
    "<leader>relua",
    ":luafile %<CR>:echo 'Luafie has been reloaded'<CR>",
    ns)

-- Script: speeds keyboard latency
keymap("n",
    "<leader>sp",
    ":! ~/.config/nvim/lua/scripts/speed.sh %<CR><CR>:echo 'Speed increased!'<CR>",
    ns)

-- Script: resize window (little)
keymap("n",
    "<leader>m",
    ":! ~/.config/nvim/lua/scripts/mini.sh %<CR><CR>",
    ns)

-- Script: resize window (full)
keymap("n",
    "<leader>f",
    ":! ~/.config/nvim/lua/scripts/full.sh %<CR><CR>",
    ns)

-- Insert snippet inside current file
keymap("n",
    "<leader>i",
    ":echo 'Press TAB to select the snippet...'<CR>:-1r ~/.config/nvim/lua/snippets/",
    ns)

-- Set current folder as master
keymap("n", "<leader>cd", ":cd %:p:h<CR>:pwd<CR>", ns)

-- Jump to the end of a line in normal mode
keymap("n", "<S-l>", "$", ns)

-- Create, close and move around tabs 
keymap("n", "<C-w>", ":wq<CR>", ns)
keymap("n", "<C-t>", ":tabnew<CR>", ns)
keymap("n", "<A-h>", ":tabprevious<CR>", ns)
keymap("n", "<A-l>", ":tabnext<CR>", ns)
keymap("n", "<leader>e", ":Lexplore<CR>:vertical resize 30<CR>", ns)

-- VISUAL ---------------------------------------------------------------------

-- Jump for the end of the line
keymap("v", "<S-l>", "$", ns)

-- Move lines around
keymap("v", "<A-j>", ":m '>+1<CR>gv=gv", ns)
keymap("v", "<A-k>", ":m '<-2<CR>gv=gv", ns)

-- TERMINAL -------------------------------------------------------------------

-- Quit terminal insert mode faster
keymap("t", "<Esc>", "<C-\\><C-n>", ns)

