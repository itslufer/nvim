local g = vim.g

local opt = vim.opt

-- Leader
g.mapleader = ' '

-- Encoding
opt.fileencoding = "utf-8"

-- Formating
vim.wo.number = true
-- vim.wo.colorcolumn = '80'
opt.wrap = false
opt.showmatch = true
opt.showmode = true
opt.splitright = true
opt.splitbelow = true
opt.scrolloff = 6 
opt.sidescrolloff = 6 
opt.expandtab = true
opt.shiftwidth = 2
opt.numberwidth = 2
opt.tabstop = 2
opt.cmdheight = 1
opt.showtabline = 2

-- General
opt.mouse = 'a'
opt.timeoutlen = 1000
opt.updatetime = 300
opt.ignorecase = true
opt.hlsearch = true
opt.list = true

-- Identation
opt.autoindent = true
opt.smartindent = true
opt.breakindent = true

-- Sounds
opt.errorbells = false
opt.visualbell = false

-- Save and History 
opt.swapfile = false
opt.hidden = true
opt.history = 100
opt.backup = false
opt.writebackup = false
opt.undofile = true

