local Plug = vim.fn['plug#']
vim.call('plug#begin', '/home/henrique/.config/nvim/plugged')

-- Colors and Highlight
Plug 'sheerun/vim-polyglot'
Plug 'nvim-lualine/lualine.nvim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'folke/tokyonight.nvim'
Plug 'EdenEast/nightfox.nvim'
Plug 'morhetz/gruvbox'
Plug 'karb94/neoscroll.nvim'
Plug 'rebelot/kanagawa.nvim'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'junegunn/goyo.vim'

vim.call('plug#end')
