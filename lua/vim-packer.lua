return require('packer').startup(function()

  use { "wbthomason/packer.nvim" }
  use 'nvim-lua/plenary.nvim'
 
  -- Lsp stuff
  use 'williamboman/nvim-lsp-installer'
  use 'neovim/nvim-lspconfig'
  
  -- Completion
  use 'hrsh7th/nvim-cmp' -- Autocompletion plugin
  use 'hrsh7th/cmp-nvim-lsp' -- LSP source for nvim-cmp
  use 'saadparwaiz1/cmp_luasnip' -- Snippets source for nvim-cmp
  use 'L3MON4D3/LuaSnip' -- Snippets plugin
  use 'onsails/lspkind.nvim'

  use 'windwp/nvim-autopairs'

  use {
    "mcchrish/zenbones.nvim",
    -- Optionally install Lush. Allows for more configuration or extending the colorscheme
    -- If you don't want to install lush, make sure to set g:zenbones_compat = 1
    -- In Vim, compat mode is turned on as Lush only works in Neovim.
    requires = "rktjmp/lush.nvim"
  }

  use 'NTBBloodbath/doom-one.nvim'

end)
