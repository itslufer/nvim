-- Servers config

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)

require("nvim-lsp-installer").setup {}
local lspconfig = require("lspconfig")

    -- Python
    lspconfig.pyright.setup{capabilities = capabilities}

    -- Lua
    lspconfig.sumneko_lua.setup{
    capabilities = capabilities,
    -- Telling to Lua that 'vim' is global
    settings = {
      Lua = {
          diagnostics = {
        globals = {'vim'},
        },
       },
      },
    }

    -- Typescript and Javascript
    lspconfig.tsserver.setup {capabilities = capabilities}

    -- HTML
    lspconfig.html.setup {capabilities = capabilities}

    -- C and C++ 
    -- lspconfig.ccls.setup {capabilities = capabilities}
    -- (This server doesn't work for some reason...)
    lspconfig.clangd.setup {capabilities = capabilities}

    -- C#
    lspconfig.csharp_ls.setup {capabilities = capabilities}

-- --------------------------------------------------------------------------

-- Keymaps
local keymap = vim.api.nvim_set_keymap
local ns = { noremap = true, silent = true }
-- Enable and disable LSP
keymap("n", "<leader>on", ":LspStart<CR>", ns)
keymap("n", "<leader>off", ":LspStop<CR>", ns)

